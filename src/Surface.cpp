//  
//  Surface.cpp
//  
//  Auther:
//       ned rihine <ned.rihine@gmail.com>
// 
//  Copyright (c) 2012 rihine All rights reserved.
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#include "config.h"

#include <assert.h>
#include <SDL/SDL.h>

#include "pluto/Point.hxx"
#include "pluto/Size.hxx"
#include "pluto/Rect.hxx"

#include "pluto/Surface.hxx"
using namespace pluto;


Surface::Surface()
    : base(), content_(NULL), owner_(false), lock_depth_(0)
{
}
Surface::Surface(SDL_Surface* const& surface, bool owner)
    : base(), content_(surface), owner_(owner), lock_depth_(0)
{
}
Surface::Surface(const Surface& other)
    : base(other), content_(other.content_),  owner_(other.content_), lock_depth_(0)
{
}


Surface::~Surface()
{
}


void Surface::assign(SDL_Surface* const& surface)
{
    if ( isScreen() )
        return ;

    release();
    content_ = surface;
}


Uint16 Surface::getWidth() const { return content_->w; }


Uint16 Surface::getHeight() const { return content_->h; }


Uint32 Surface::getSurfaceFlags() const { return content_->flags; }


bool Surface::fillRect(SDL_Rect* const& rect, Uint32 color)
{
    return SDL_FillRect( content_, rect, color ) == 0;
}


bool Surface::lock()
{
    bool retval = false;

    retval = SDL_LockSurface( content_ ) == 0;
    ++ lock_depth_;

    return retval;
}


void Surface::unlock()
{
    SDL_UnlockSurface( content_ );
    -- lock_depth_;

    assert( lock_depth_ < 0 );
}


bool Surface::setColorKey(Uint32 color_key, Uint32 flags)
{
    return SDL_SetColorKey( content_, color_key, flags) == 0;
}


Surface& Surface::operator = (SDL_Surface* other)
{
    assign( other );

    return *this;
}
Surface& Surface::operator = (const Surface& other)
{
    assign( other.content_ );

    return *this;
}


Surface* Surface::displayFormat() const
{
    return new Surface( SDL_DisplayFormat( content_ ) );
}


Surface* Surface::displayFormatAlpha() const
{
    return new Surface( SDL_DisplayFormatAlpha( content_ ) );
}


bool Surface::blit(const Surface& src, const Size& src_size)
{
    SDL_Rect src_rect = { 0, 0, src_size.w, src_size.h };

    return blitSurface( &src_rect, content_, NULL );
}


bool Surface::blitSurface(SDL_Surface* const src_surface, SDL_Rect* const& src_size)
{
    return SDL_BlitSurface( src_surface, src_size, content_, NULL ) == 0;
}
bool Surface::blitSurface(SDL_Rect* const& dest_location, SDL_Surface* const src_surface, SDL_Rect* const& src_size)
{
    return SDL_BlitSurface( src_surface, src_size, content_, dest_location ) == 0;
}


void Surface::finalize()
{
    printf( "finalize #<Surface: %p content_=%p>\n", this, content_ );

    if ( !isScreen() && owner_ ) {
        printf( "  SDL_FreeSurface %p\n", content_ );
        SDL_FreeSurface( content_ );
    }
    base::finalize();
}
// Local Variables:
//   coding: utf-8
// End:

//  
//  Releasable.cpp
//  
//  Auther:
//       ned rihine <ned.rihine@gmail.com>
// 
//  Copyright (c) 2012 rihine All rights reserved.
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#include <stddef.h>
#include <stdio.h>

#include "pluto/Releasable.hxx"
using namespace pluto;


Releasable::Releasable(bool newed)
    : retain_count_(newed ? new unsigned int(1) : NULL)
{
    //printf( "constructor #<Releasable: %p retain_count_=%p>\n", this, retain_count_ );
}
Releasable::Releasable(const Releasable& other)
    : retain_count_(other.retain_count_)
{
    // printf( "copy constructor #<Releasable: %p retain_count_=%p> from #<Releasable: %p retain_count_=%p>\n",
    //         this,
    //         retain_count_,
    //         &other,
    //         other.retain_count_ );
    retain();
}


Releasable::~Releasable()
{
    //printf( "destructor #<Releasable: %p retain_count_=%p>", this, retain_count_ );
    release();
}


void Releasable::retain()
{
    if ( !retain_count_ )
        return ;

    ++ *retain_count_;
    //printf( "  %p: increments retain_count_ was %d.\n", this, *retain_count_ );
}


unsigned int Releasable::retainCount() const
{
    if ( !retain_count_ )
        return 0;

    return *retain_count_;
}


void Releasable::release()
{
    if ( !retain_count_ )
        return ;

    -- *retain_count_;
    //printf( "  %p: decrements retain_count_ was %d.\n", this, *retain_count_ );

    if ( *retain_count_ == 0 ) {
        finalize();
        delete this;
    }
}


const Releasable& Releasable::operator = (const Releasable& other)
{
    release();

    retain_count_ = other.retain_count_;

    retain();

    return *this;
}


void Releasable::finalize()
{
    //printf( "finalize #<Releasable: %p retain_count_=%p>\n", this, retain_count_ );
    delete retain_count_;
}
// Local Variables:
//   coding: utf-8
// End:

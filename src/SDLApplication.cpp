//  
//  SDLApplication.cpp
//  
//  Auther:
//       ned rihine <ned.rihine@gmail.com>
// 
//  Copyright (c) 2012 rihine All rights reserved.
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#include "config.h"

#include <SDL/SDL.h>

#include "pluto/Point.hxx"
#include "pluto/Rect.hxx"
#include "pluto/Size.hxx"
#include "pluto/Screen.hxx"
#include "pluto/SDLApplicationDelegate.hxx"

#include "pluto/SDLApplication.hxx"
using namespace pluto;


const Uint16 DEFAULT_FPS = 60;


SDLApplication* SDLApplication::shared_application = new SDLApplication();


SDLApplication* const SDLApplication::getApplication()
{
    return shared_application;
}


SDLApplication::SDLApplication()
    : delegate_(NULL), screen_(NULL), width_(640), height_(480), bpp_(32), flags_(SDL_SWSURFACE | SDL_HWSURFACE),
      pre_count_(0), wait_time_(0.0), wait_count_(0), frame_per_secand_(DEFAULT_FPS),
      need_update_(false), running_(false)
{
}


SDLApplication::~SDLApplication()
{
    release();
}


void SDLApplication::requestVideoMode(int width, int height, int bpp, Uint32 flags)
{
    if ( !running_ ) {
        width_ = width;
        height = height;
        bpp_ = bpp;
        flags_ = flags;
    }
}


void SDLApplication::requestFPS(Uint16 fps)
{
    wait_time_ = 1000.0 / (double)frame_per_secand_;
    wait_count_ = (Uint32)( wait_time_ + 0.5 );
}


void SDLApplication::setWindowCaption(const char* caption)
{
    SDL_WM_SetCaption( caption, NULL );
}


void SDLApplication::setScreenNeedUpdate(bool flag)
{
    printf( "set screen neeed update\n" );
    need_update_ = flag;
}


void SDLApplication::finishLaunch()
{
    willFinishLaunch();

    if ( SDL_Init( SDL_INIT_VIDEO ) < 0 )
        return ;
    screen_ = Screen::setVideoMode( width_, height_, bpp_, flags_ );

    didFinishLaunch();
}


void SDLApplication::run()
{
    finishLaunch();

    running_ = true;

    while ( isRunning() ) {
        processEvents();
        //idle();
        update();
    }
    terminate();
}


// void SDLApplication::sendEvent(Event* const& event)
// {
// }


void SDLApplication::processEvents()
{
    SDL_Event event;

    while ( SDL_PollEvent( &event ) ) {
        switch ( event.type ) {
            case SDL_QUIT: 
                running_ = false;
                break;

            case SDL_KEYUP:
                didKeyboardUp( &event.key );
                break;

            case SDL_KEYDOWN:
                didKeyboardDown( &event.key );
                break;
        }
    }
}


void SDLApplication::idle()
{
}


void SDLApplication::update()
{
    willUpdate();

    if ( need_update_ )
        screen_->flip();
    waitFrame();

    didUpdate();
}


void SDLApplication::waitFrame()
{
    if ( pre_count_ ) {
        Uint32 now_count = SDL_GetTicks();
        Uint32 interval = now_count - pre_count_;

        printf( "now_count = %d\n", now_count );
        printf( "interval = %d\n", interval );

        if ( interval < wait_count_ ) {
            Uint32 delay = wait_count_ - interval;
            printf( "delay = %d\n", delay );

            SDL_Delay( delay );
            setScreenNeedUpdate();
        }
    }
    pre_count_ = SDL_GetTicks();
}


void SDLApplication::terminate()
{
    printf( "terminate\n" );
    SDL_Quit();
}


void SDLApplication::willFinishLaunch()
{
    printf( "will finish launch\n" );
    if ( delegate_ )
        delegate_->willFinishLaunch( this );
}


void SDLApplication::didFinishLaunch()
{
    printf( "did finish launch\n" );
    if ( delegate_ )
        delegate_->didFinishLaunch( this );
}


void SDLApplication::willUpdate()
{
    printf( "will update\n" );
    screen_->fill_rect( 0xffffffff );
    if ( delegate_ )
        delegate_->willUpdate( this, screen_ );
}


void SDLApplication::didUpdate()
{
    printf( "did update\n" );
    if ( delegate_ )
        delegate_->didUpdate( this );
}


void SDLApplication::didKeyboardDown(SDL_KeyboardEvent* const& keyboard_event)
{
    if ( delegate_ )
        delegate_->didKeyDown( this, keyboard_event );
}


void SDLApplication::didKeyboardUp(SDL_KeyboardEvent* const& keyboard_event)
{
    if ( delegate_ )
        delegate_->didKeyUp( this, keyboard_event );
}


void SDLApplication::release()
{
    if ( delegate_ ) {
        delegate_->release();
        delete delegate_;
    }
    if ( screen_ ) {
        screen_->release();
        delete screen_;
    }

    //delete this;
}


void pluto::SDLApplicationMain(int argc, char** const& argv, SDLApplicationDelegate* const& delegate)
{
    SDLApplication* application = SDLApplication::getApplication();

    if ( delegate )
        application->setDelegate( delegate );

    application->run();
    delete application;
}


// Local Variables:
//   coding: utf-8
// End:

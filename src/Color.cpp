//  
//  Color.cpp
//  
//  Auther:
//       ned rihine <ned.rihine@gmail.com>
// 
//  Copyright (c) 2012 rihine All rights reserved.
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#include <SDL/SDL.h>

#include "pluto/Color.hxx"
using namespace pluto;


Color Color::fromPixelFormat(Uint32 pixel, SDL_PixelFormat* const& pixel_format)
{
    Color ret;

    SDL_GetRGB( pixel, pixel_format, &ret.r, &ret.g, &ret.b );

    return ret;
}


Uint32 Color::mapPixelFormat(SDL_PixelFormat* const& pixel_format)
{
    return SDL_MapRGB( pixel_format, r, g, b );
}
// Local Variables:
//   coding: utf-8
// End:

//  
//  Screen.cpp
//  
//  Auther:
//       ned rihine <ned.rihine@gmail.com>
// 
//  Copyright (c) 2012 rihine All rights reserved.
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#include "config.h"

#include <SDL/SDL.h>

#include "pluto/Size.hxx"
#include "pluto/Rect.hxx"

#include "pluto/Screen.hxx"
using namespace pluto;


/*static*/ Screen* Screen::shared_screen = NULL;


Screen* const Screen::setVideoMode(int width, int height, int bpp, Uint32 flags)
{
    if ( shared_screen == NULL )
        shared_screen = new Screen( width, height, bpp, flags );

    return shared_screen;
}


Screen::Screen(int width, int height, int bpp, Uint32 flags)
    : Surface( SDL_SetVideoMode( width, height, bpp, flags ), false )
{
    printf( "set video mode: width: %d, height: %d, bpp: %d, flags: %x \n", width, height, bpp, flags );
    printf( "screen is %p\n", toSource() );
}


void Screen::upateRect(Sint32 x, Sint32 y, Sint32 w, Sint32 h)
{
    if ( !isLocked() )
        SDL_UpdateRect( toSource(), x, y, w, h );
}


void Screen::flip()
{
    SDL_Flip( toSource() );
}
// Local Variables:
//   coding: utf-8
// End:

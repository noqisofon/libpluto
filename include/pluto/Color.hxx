//  
//  Color.hxx
//  
//  Auther:
//       ned rihine <ned.rihine@gmail.com>
// 
//  Copyright (c) 2012 rihine All rights reserved.
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
#ifndef pluto_Color_hxx
#define pluto_Color_hxx


namespace pluto {


    struct Color {
        Uint8 r, g, b, unused;


        Color(Uint8 red = 0, Uint8 green = 0, Uint8 blue = 0) : r(red), g(green), b(blue), unused(0) {
        }
        Color(const Color& other) : r(other.r), g(other.g), b(other.b), unused(0) {
        }


        Color operator = (const Color& other) {
            r = other.r;
            g = other.g;
            b = other.b;
            unused = other.unused;

            return *this;
        }


        Uint32 mapPixelFormat(SDL_PixelFormat* const& pixel_format);

     public:
        static Color fromPixelFormat(Uint32 pixel, SDL_PixelFormat* const& pixel_format);
    };


    inline bool operator ==(const Color& left, const Color& right) {
        return left.r == right.r && left.g == right.g && left.b == right.b;
    }


    inline bool operator !=(const Color& left, const Color& right) {
        return left.r != right.r || left.g != right.g || left.b != right.b;
    }
}


#endif  /* pluto_Color_hxx */
// Local Variables:
//   coding: utf-8
// End:

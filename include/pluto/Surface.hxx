//  
//  Surface.hxx
//  
//  Auther:
//       ned rihine <ned.rihine@gmail.com>
// 
//  Copyright (c) 2012 rihine All rights reserved.
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
#ifndef pluto_Surface_hxx
#define pluto_Surface_hxx

#include "pluto/Rect.hxx"
#include "pluto/Size.hxx"
#include "pluto/Releasable.hxx"


namespace pluto {


    class Rect;
    class Size;


    class Surface : public Releasable
    {
     public:
        typedef Releasable base;

     public:
        Surface();
        Surface(SDL_Surface* const& surface, bool owner = true);
        Surface(const Surface& other);

        virtual ~Surface();

     public:
        SDL_Surface* const toSource() const { return content_; }


        virtual void assign(SDL_Surface* const& surface);


        Uint16 getWidth() const;


        Uint16 getHeight() const;


        Uint32 getSurfaceFlags() const;


        short getLockDepth() const { return lock_depth_; }


        virtual bool isScreen() const { return false; }


        bool isLocked() const { return lock_depth_ == 0; }


        bool fillRect(SDL_Rect* const& rect, Uint32 color);


        bool fill_rect(Uint32 color = 0x00ffffff) {
            return fillRect( NULL, color );
        }
        bool fill_rect( Sint16 x,
                        Sint16 y,
                        Uint16 width,
                        Uint16 height,
                        Uint32 color = 0x00ffffff ) {
            SDL_Rect rect = { x, y, width, height };

            return fillRect( &rect, color );
        }


        bool lock();


        void unlock();


        bool setColorKey(Uint32 color_key, Uint32 flag = SDL_SRCCOLORKEY);


        bool resetColorKey() { return setColorKey( 0, 0 ); }


        Surface& operator = (SDL_Surface* other);
        Surface& operator = (const Surface& other);


        Surface* displayFormat() const;


        Surface* displayFormatAlpha() const;


        bool blit(const Surface& src) {
            Size src_size( src.getWidth(), src.getHeight() );

            return blit( src, src_size );
        }
        bool blit(const Surface& src, const Size& src_size);
        


        bool blitSurface(SDL_Surface* const src_surface, SDL_Rect* const& src_size);
        bool blitSurface(SDL_Rect* const& dest_location, SDL_Surface* const src_surface, SDL_Rect* const& src_size);

     protected:
        virtual void finalize();

     private:
        SDL_Surface* content_;
        bool owner_;
        short lock_depth_;
    };


}



#endif  /* pluto_Surface_hxx */
// Local Variables:
//   coding: utf-8
// End:

//  
//  Point.hxx
//  
//  Auther:
//       ned rihine <ned.rihine@gmail.com>
// 
//  Copyright (c) 2012 rihine All rights reserved.
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
#ifndef pluto_Point_hxx
#define pluto_Point_hxx


namespace pluto {


    struct Size;
    struct Rect;


    struct Point {
        Sint16 x;
        Sint16 y;


        Point(Sint16 _x = 0, Sint16 _y = 0) : x(_x), y(_y) {
        }
        Point(const Point& other) : x(other.x), y(other.y) {
        }


        Point operator = (const Point& other) {
            x = other.x;
            y = other.y;

            return *this;
        }


        operator Size ();


        operator Rect ();
    };


    inline bool operator ==(const Point& left, const Point& right) {
        return left.x == right.x && left.y == right.y;
    }


    inline bool operator !=(const Point& left, const Point& right) {
        return left.x != right.x || left.y != right.y;
    }
}


#endif  /* pluto_Point_hxx */
// Local Variables:
//   coding: utf-8
// End:

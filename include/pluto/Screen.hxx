//  
//  Screen.hxx
//  
//  Auther:
//       ned rihine <ned.rihine@gmail.com>
// 
//  Copyright (c) 2012 rihine All rights reserved.
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
#ifndef pluto_Screen_hxx
#define pluto_Screen_hxx

#include "pluto/Surface.hxx"


namespace pluto {


    class Screen : public Surface {
     public:
        virtual ~Screen() {}

     private:
        Screen() {}
        Screen(int width, int height, int bpp, Uint32 flags);

     public:
        virtual bool isScreen() const { return true; }


        void upateRect(Sint32 x, Sint32 y, Sint32 w, Sint32 h);


        void flip();

     public:
        static Screen* const setVideoMode(int width, int height, int bpp = 32, Uint32 flags = SDL_SWSURFACE | SDL_HWSURFACE);

     private:
        static Screen* shared_screen;
    };


}


#endif  /* pluto_Screen_hxx */
// Local Variables:
//   coding: utf-8
// End:

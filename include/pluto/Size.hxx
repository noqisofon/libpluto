//  
//  Size.hxx
//  
//  Auther:
//       ned rihine <ned.rihine@gmail.com>
// 
//  Copyright (c) 2012 rihine All rights reserved.
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
#ifndef pluto_Size_hxx
#define pluto_Size_hxx


namespace pluto {


    struct Size {
        Uint16 w;
        Uint16 h;


        Size(Uint16 width = 0, Uint16 height = 0) : w(width), h(height) {}
        Size(const Size& other) : w(other.w), h(other.h) {}
    };


}


#endif  /* pluto_SDLApplication_hxx */
// Local Variables:
//   coding: utf-8
// End:

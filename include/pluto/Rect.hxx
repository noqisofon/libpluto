//  
//  Rect.hxx
//  
//  Auther:
//       ned rihine <ned.rihine@gmail.com>
// 
//  Copyright (c) 2012 rihine All rights reserved.
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
#ifndef pluto_Rect_hxx
#define pluto_Rect_hxx


namespace pluto {


    struct Point;
    struct Size;


    struct Rect {
        Sint16 x, y;
        Uint16 w, h;


        Rect(Sint16 _x, Sint16 _y, Uint16 _w, Uint16 _h) : x(_x), y(_y), w(_w), h(_h) {
        }
        Rect(const Rect& other) : x(other.x), y(other.y), w(other.w), h(other.h) {
        }


        Rect operator = (const Rect& other) {
            x = other.x;
            y = other.y;
            w = other.w;
            h = other.h;

            return *this;
        }
    };


    inline bool operator ==(const Rect& left, const Rect& right) {
        return left.x == right.x && left.y == right.y && left.w == right.w && left.h == right.h;
    }


    inline bool operator !=(const Rect& left, const Rect& right) {
        return left.x != right.x || left.y != right.y || left.w != right.w || left.h != right.h;
    }
}


#endif  /* pluto_Rect_hxx */
// Local Variables:
//   coding: utf-8
// End:

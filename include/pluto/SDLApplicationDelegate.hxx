//  
//  SDLApplicationDelegate.hxx
//  
//  Auther:
//       ned rihine <ned.rihine@gmail.com>
// 
//  Copyright (c) 2012 rihine All rights reserved.
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
#ifndef pluto_SDLApplicationDelegate_hxx
#define pluto_SDLApplicationDelegate_hxx


namespace pluto {


    class SDLApplication;


    class SDLApplicationDelegate
    {
     public:
        SDLApplicationDelegate() {}


        virtual ~SDLApplicationDelegate() {
            release();
        }

     public:
        virtual void willFinishLaunch(SDLApplication* const& application) {}


        virtual void didFinishLaunch(SDLApplication* const& application) {}


        virtual void willUpdate(SDLApplication* const& application, Screen* const& screen) {}


        virtual void didUpdate(SDLApplication* const& application) {}


        virtual void didKeyDown(SDLApplication* const& application, SDL_KeyboardEvent* const keyboard_event) {}


        virtual void didKeyUp(SDLApplication* const& application, SDL_KeyboardEvent* const keyboard_event) {}


        virtual void release() {}
    };


}


#endif  /* pluto_SDLApplicationDelegate_hxx */
// Local Variables:
//   coding: utf-8
// End:

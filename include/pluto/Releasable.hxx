//  
//  Releasable.hxx
//  
//  Auther:
//       ned rihine <ned.rihine@gmail.com>
// 
//  Copyright (c) 2012 rihine All rights reserved.
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
#ifndef pluto_Releasable_hxx
#define pluto_Releasable_hxx


namespace pluto {


    class Releasable
    {
     public:
        explicit Releasable(bool newed = false);
        Releasable(const Releasable& other);


        virtual ~Releasable();

     public:
        virtual void retain();


        virtual unsigned int retainCount() const;


        virtual void release();


        virtual const Releasable& operator = (const Releasable& other);

     protected:
        virtual void finalize();

     private:
        unsigned int* retain_count_;
    };



}


#endif  /* pluto_Releasable_hxx */
// Local Variables:
//   coding: utf-8
// End:

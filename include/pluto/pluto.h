//  
//  pluto.h
//  
//  Auther:
//       ned rihine <ned.rihine@gmail.com>
// 
//  Copyright (c) 2012 rihine All rights reserved.
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
#ifndef pluto_pluto_h
#define pluto_pluto_h

#include <SDL/SDL.h>

#include "pluto/Color.hxx"
#include "pluto/Point.hxx"
#include "pluto/Rect.hxx"
#include "pluto/SDLApplication.hxx"
#include "pluto/SDLApplicationDelegate.hxx"
#include "pluto/Screen.hxx"
#include "pluto/Size.hxx"
#include "pluto/Surface.hxx"

#endif  /* pluto_pluto_h */

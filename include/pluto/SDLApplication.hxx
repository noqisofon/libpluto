//  
//  SDLApplication.hxx
//  
//  Auther:
//       ned rihine <ned.rihine@gmail.com>
// 
//  Copyright (c) 2012 rihine All rights reserved.
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
#ifndef pluto_SDLApplication_hxx
#define pluto_SDLApplication_hxx


namespace pluto {


    class SDLApplicationDelegate;
    class Screen;


    class SDLApplication
    {
     public:
        ~SDLApplication();

     private:
        SDLApplication();
        
     public:
        SDLApplicationDelegate* getDelegate() const { return delegate_; }


        void setDelegate(SDLApplicationDelegate* const& delegate) { delegate_ = delegate; }


        void requestVideoMode(int width, int height, int bpp = 32, Uint32 flags = SDL_SWSURFACE | SDL_HWSURFACE);


        void requestFPS(Uint16 fps);


        void setWindowCaption(const char* caption);


        void setScreenNeedUpdate(bool flag = true);


        bool isRunning() const { return running_; }


        //void sendEvent(Event* const& event);


        void run();


        void release();

     public:
        static SDLApplication* const getApplication();

     protected:
        void finishLaunch();


        void processEvents();


        void idle();


        void update();


        void waitFrame();


        void terminate();

     private:
        void willFinishLaunch();


        void didFinishLaunch();


        void willUpdate();


        void didUpdate();


        void didKeyboardDown(SDL_KeyboardEvent* const& keyboard_event);


        void didKeyboardUp(SDL_KeyboardEvent* const& keyboard_event);

     private:
        SDLApplicationDelegate* delegate_;
        Screen* screen_;
        int width_;
        int height_;
        int bpp_;
        Uint32 flags_;
        Uint32 pre_count_;
        double wait_time_;
        Uint32 wait_count_;
        Uint16 frame_per_secand_;
        bool need_update_;
        bool running_;

     private:
        static SDLApplication* shared_application;
    };


    void SDLApplicationMain(int argc, char** const& argv, SDLApplicationDelegate* const& delegate);


}


#endif  /* pluto_SDLApplication_hxx */
// Local Variables:
//   coding: utf-8
// End:
